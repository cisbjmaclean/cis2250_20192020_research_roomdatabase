package com.example.roomdatabase;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface CourtDAO {

    @Insert
    public long addCourt(Court court);

    @Update
    public void updateCourt(Court court);
    @Delete
    public void deleteCourt(Court court);

    @Query("select * from court")
    public List<Court> getCourt();

    @Query("select * from court where id =:id")
    public Court getCourtFromId(int id);
}
