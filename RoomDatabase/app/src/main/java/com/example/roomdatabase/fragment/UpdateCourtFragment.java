package com.example.roomdatabase.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.roomdatabase.Activity.DatabaseActivity;
import com.example.roomdatabase.Court;
import com.example.roomdatabase.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateCourtFragment extends Fragment {

    private EditText CourtId,CourtName, CourtNum, CourtType;
    private Button BtnUpdate;
    public UpdateCourtFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_update_court, container, false);
      CourtId = view.findViewById(R.id.cid);
       CourtName = view.findViewById(R.id.cname);
       CourtNum = view.findViewById(R.id.cnum);
       CourtType = view.findViewById(R.id.ctype);
       BtnUpdate = view.findViewById(R.id.updatebtn);
        BtnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = Integer.parseInt(CourtId.getText().toString());
                String name = CourtName.getText().toString();
                int num = Integer.parseInt(CourtNum.getText().toString());
                int type = Integer.parseInt(CourtType.getText().toString());

                Court court = new Court();
                court.setCourtId(id);
                court.setCourtName(name);
                court.setCourtNum(num);
                court.setCourtType(type);

                DatabaseActivity.myAppDatabase.courtDAO().updateCourt(court);
                Toast.makeText(getActivity(),"court updated succesfully",Toast.LENGTH_SHORT).show();
                CourtId.setText("");
                CourtName.setText("");
                CourtNum.setText("");
                CourtType.setText("");
            }
        });

        return view;
    }

}
