package com.example.roomdatabase.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.roomdatabase.Court;
import com.example.roomdatabase.Activity.DatabaseActivity;
import com.example.roomdatabase.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class DeleteCourtFragment extends Fragment {

    private EditText textCourtId;
    private Button DeleteButton;
    public DeleteCourtFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delete_court, container, false);
        textCourtId = view.findViewById(R.id.courtId);
        DeleteButton = view.findViewById(R.id.btnDelete);

        DeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = Integer.parseInt(textCourtId.getText().toString());
                Court court = new Court();
                court.setCourtId(id);
                DatabaseActivity.myAppDatabase.courtDAO().deleteCourt(court);

                Toast.makeText(getActivity(),"User Deleted successfully",Toast.LENGTH_SHORT).show();
                textCourtId.setText("");
            }
        });
        return view;
    }

}
