package com.example.roomdatabase.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.roomdatabase.Activity.DatabaseActivity;
import com.example.roomdatabase.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    private Button BtnAddCourt;
    private Button BtnViewCourt;
    private Button BtnDeleteCourt;
    private Button BtnUpdateCourt;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        BtnAddCourt = view.findViewById(R.id.add);
        BtnViewCourt =  view.findViewById(R.id.view);
        BtnDeleteCourt =  view.findViewById(R.id.delete);
        BtnUpdateCourt = view.findViewById(R.id.update);
        BtnAddCourt.setOnClickListener(this);
        BtnViewCourt.setOnClickListener(this);
        BtnDeleteCourt.setOnClickListener(this);
        BtnUpdateCourt.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.add:
                DatabaseActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container,new AddCourtFragment()).addToBackStack(null).commit();
                break;
            case R.id.view:
                DatabaseActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container,new ReadCourtFragment()).addToBackStack(null).commit();
                break;
            case R.id.delete:
                DatabaseActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container,new DeleteCourtFragment()).addToBackStack(null).commit();
                break;
            case R.id.update:
                DatabaseActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container,new UpdateCourtFragment()).addToBackStack(null).commit();
                break;
        }
    }
}
