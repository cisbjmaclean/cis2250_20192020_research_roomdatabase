package com.example.roomdatabase.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.roomdatabase.Court;
import com.example.roomdatabase.Activity.DatabaseActivity;
import com.example.roomdatabase.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddCourtFragment extends Fragment {

    private EditText courtName,courtNum,courtType;
    private Button btnSave;

    public AddCourtFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_court, container, false);

        courtName = view.findViewById(R.id.courtName);
        courtNum = view.findViewById(R.id.courtNum);
        courtType  = view.findViewById(R.id.courtType);
        btnSave = view.findViewById(R.id.save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cName = courtName.getText().toString();
                int cNum = Integer.parseInt(courtNum.getText().toString());
                int cType = Integer.parseInt(courtType.getText().toString());
                Court court = new Court();
                court.setCourtName(cName);
                court.setCourtNum(cNum);
                court.setCourtType(cType);

                DatabaseActivity.myAppDatabase.courtDAO().addCourt(court);
                Toast.makeText(getActivity(),"court added succesfully",Toast.LENGTH_SHORT).show();

                courtName.setText("");
                courtNum.setText("");
                courtType.setText("");
            }
        });

        return view;
    }

}
