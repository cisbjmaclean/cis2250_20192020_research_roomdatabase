package com.example.roomdatabase;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "court")
public class Court {

  //  public static final String TABLE_NAME = "court";

 //   public static final String COLUMN_ID ="id";

//    public static final String COLUMN_COURTNUM = "courtNumber";

 //   public static final String COLUMN_COURTNAME = "courtName";

  //  public static final String COLUMN_COURTTYPE = "courtType";

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private long courtId;
    @ColumnInfo(name = "courtNumber")
    private int courtNum;
    @ColumnInfo(name = "courtName")
    private String courtName;
    @ColumnInfo(name = "courtType")
    private int courtType;

    @Ignore
    public Court()
    {

    }

    public Court(long courtId, int courtNum, String courtName, int courtType) {
        this.courtId = courtId;
        this.courtNum = courtNum;
        this.courtName = courtName;
        this.courtType = courtType;
    }

    public long getCourtId() {
        return courtId;
    }

    public void setCourtId(long courtId) {
        this.courtId = courtId;
    }

    public int getCourtNum() {
        return courtNum;
    }

    public void setCourtNum(int courtNum) {
        this.courtNum = courtNum;
    }

    public String getCourtName() {
        return courtName;
    }

    public void setCourtName(String courtName) {
        this.courtName = courtName;
    }

    public int getCourtType() {
        return courtType;
    }

    public void setCourtType(int courtType) {
        this.courtType = courtType;
    }

    // create table SQL query

}
