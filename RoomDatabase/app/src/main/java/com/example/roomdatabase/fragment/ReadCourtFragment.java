package com.example.roomdatabase.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.roomdatabase.Court;
import com.example.roomdatabase.Activity.DatabaseActivity;
import com.example.roomdatabase.R;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReadCourtFragment extends Fragment {

    private TextView DisplayText;
    public ReadCourtFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_read_court, container, false);
        DisplayText = view.findViewById(R.id.display_txt);
        List<Court> courts = DatabaseActivity.myAppDatabase.courtDAO().getCourt();
        String info = "";
        for(Court crt : courts)
        {
            long id = crt.getCourtId();
            String name = crt.getCourtName();
            int num = crt.getCourtNum();
            int type = crt.getCourtType();

            info += "\n\n"+"Id: "+id+"\n Court Name: "+name+"" +
                    "\n Court Number: "+num+
                    "\n Court type: "+type;
        }

        DisplayText.setText(info);
        return view;
    }

}
