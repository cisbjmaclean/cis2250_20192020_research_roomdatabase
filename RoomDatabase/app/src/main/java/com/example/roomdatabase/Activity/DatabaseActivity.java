package com.example.roomdatabase.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.room.Room;

import android.os.Bundle;

import com.example.roomdatabase.MyAppDatabase;
import com.example.roomdatabase.R;
import com.example.roomdatabase.fragment.HomeFragment;

public class DatabaseActivity extends AppCompatActivity {
    public static FragmentManager fragmentManager;
    public static MyAppDatabase myAppDatabase;
    //private CourtDatabase courtDatabase;
   // private RecyclerView recyclerView;
  //  private RecyclerView.Adapter mAdapter;
 //   private RecyclerView.LayoutManager mLayoutManager;
 //   private ArrayList<Court> courtList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);
        fragmentManager = getSupportFragmentManager();
       // Room.databaseBuilder(getApplicationContext(),Court.class,"courtdb")
        myAppDatabase = Room.databaseBuilder(getApplicationContext(),MyAppDatabase.class,"courtdb").allowMainThreadQueries().build();
        if(findViewById(R.id.fragment_container)!=null)
        {
            if(savedInstanceState != null)
            {
                return;
            }
            fragmentManager.beginTransaction().add(R.id.fragment_container,new HomeFragment()).commit();
        }
//        ArrayList<Court> court = new ArrayList<>();
//        court.add(new Court(1,1,"court1",3));
//        court.add(new Court(2,1,"court1",3));
//        court.add(new Court(3,1,"court1",3));

   //     myAppDatabase = Room.databaseBuilder(getApplicationContext(),CourtDatabase.class,"cis2232_fitness").allowMainThreadQueries().build();
   //     courtList.addAll(courtDatabase.getCourtDAO().getCourt());
//    recyclerView.setHasFixedSize(true);
//    mLayoutManager = new LinearLayoutManager(this);
//    mAdapter = new CustomAdapter(court);
//    recyclerView.setLayoutManager(mLayoutManager);
//    recyclerView.setAdapter(mAdapter);

    }
}
